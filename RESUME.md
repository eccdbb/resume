# Resume #
Vincent Chyn

https://bitbucket.org/eccdbb/resume/src/master/

## Introduction ##

This is a live version of my resume; it may be updated from time to time.  It includes an introduction to who I am and what I have done.

### About Me ###
Born in the USA.  Undergrad at Cornell University.  Just finished my Masters studies at National Yang Ming Chiao Tung University (NYCU) (EECS program) (previously, National Chiao Tung University; https://en.wikipedia.org/wiki/National_Yang_Ming_Chiao_Tung_University).

Why do my graduate studies in NCTU?

I decided to go to Taiwan because of two reasons: cultural roots and proximity to industry.
First, my parents immigrated from Taiwan to the USA; most of my relatives are in Taiwan.  I was born and raised as an ABC.  The move felt natural, although overcoming the initial culture shock was unexpected.
Second, New York City is the center of the world (to some people), except the world is made in Asia.  Also, CU was in the middle of nowhere.  I wanted to understand the manufacturing world.

I hold a Taiwan ARC (spouse) and intend to work in/from Taiwan full time.

My interests have shifted over time.  Undergrad times: electronics, FPGA, control.  Graduate times: control, algorithms, computer vision, camera 3A, and AI.
Part of this may be due to the open source nature of the CS fields.

Nowadays, I am exploring the cloud computing world.



### Personality ###
I am a goal driven self learner.  I tend to work best when a need for improvement (of a system/component/etc.) exists and dictates the follow up work.
Alternatively, I work best when development includes multiple steps into the future, including cases of uncertainty.
I prefer to understand the fundamentals and the reason (or utility value) of something rather than follow an instruction manual.

I tend to remove or take no assumptions.  Sometimes this makes work harder, but more is learned along the way.

My weakness is that I lack confidence when lacking in knowledge or experience.  I prefer to plan carefully rather than jump straight into the swimming pool.

## Certifications ##
https://www.credly.com/users/vincent-chyn

- AWS Certified Cloud Practitioner (Jan. 2022)

## Previous working experience ##

### IT ###
Back in my college days, I did an internship at Synergy Consulting Engineers.  There I migrated the company website to an alternative provider; in the process I also set up a local mirror on a LAMP server.  I also migrated the Journyx Timesheet software to a new machine.

### MEMS Projector ###

In my 2012 and 2013 summer internships at ITRI (Tainan), I worked on a RGB laser MEMS projector.

The first summer, I implemented color management for the projector.
I set up an automated measurement system for the laser diodes and characterized them.
Then, I determined the color pipeline functions, including linearization and matrix multiplication.
I calculated the linearization LUTs in MATLAB and created a tool to generate the matrix coefficients.
This data was then sent from a PC to the projector's FPGA.  I implemented the color pipeline in Verilog/FPGA.
The end result was that the projector was capable of displaying in the sRGB color space.

A simplified block diagram is shown here:

![color pipeline](./assets/color%20pipeline.png "FPGA color pipeline")

The second summer, I designed a computationally efficient way to find corners for keystone correction.  Instead of using a filter bank and autocorrelation, I used a sorting method.
The result was that the corners could be found instantly.

### Piezo-actuator control ###
During the 2013 internship at ITRI (Tainan), I ported a piezo actuator (for camera OIS).
The original solution comprised of a microcontroller followed by an analog circuit.  The performance was not satisfactory because the timing was too loose (long latency, non-deterministic result).
I converted the PID/PWM controller implementation into a fully digital one using a FPGA development board.
The result was deterministic (cycle countable and fixed) latency.  Further, the sample frequency was raised by 10-100x.
Instead of needing to tune PID parameters, the new solution was able to use bang bang control for optimal (time) and robust (varying payloads) performance.

### Bookshelf speakers ###
Some time during college, I built a pair of bookshelf speakers.  20 lbs. per cabinet, RCF woofers and a planar tweeter.  Lesson learned: all analog components have deficiencies.  Active speakers are the way to go.

### PCB design ###
During the final years in college, I learned how to make PCBs.  I started from copper clad boards, photoresist solution, ferric chloride, and a dremel.
Eventually I found OSHPark, and used them to fabricate the boards.  In retrospect, the lesson learned is: developing hardware is expensive and slow to iterate.  In software, feedback and iteration is nearly instant.

Some of the boards I made can be seen here:

https://goo.gl/photos/iQe75M5GeCb1HnVXA

Here are some early boards, related to audio.

![early PCBs](./assets/P1100069.JPG "early PCBs made from copper clad boards and manually drilled vias")

### Masters Thesis at NCTU ###
A few months after graduating as an undergrad, I felt a lack of skills and knowledge.  I went to NCTU in Taiwan for a Masters degree.
My advisor and I decided on the topic: vehicle dynamics and model predictive control.  I thought, this should be straightforwards.
Learn both areas and implement.  Little did I know about the intricate details of: importing libraries and finding undocumented limitations, numerical optimization debugging, and the process of stepping out of failure.
The result of this process is documented in my Masters thesis.

A link to the thesis can be found below.

Some sample results related to dynamic MPC are shown here.  The goal is to find actuator commands to drive the vehicle to large slip angles and recover.

This figure shows the vehicle path.
![](./assets/MPC%20%20P7%20vehicle%20path.png)

This figure shows the velocities of the vehicle body and solver iterations taken.
![](./assets/MPC%20P7%20velocities%20and%20iter.png)

This figure shows the body slip angle.  Using MPC, it is possible to find solutions to very large slip angles and other difficult maneuvers; minimal tuning is required.
![](./assets/MPC%20P7%20slip%20angle.png)

### Multi-Language Walkie-Talkie ###
This was a 2-person team project.  We developed a multi-language walkie-talkie on AWS.  Speak in one language and listen in another.
Services used: S3, RDS, EC2, Transcribe, Translate, Polly.  See: https://github.com/duygu-bayrak/Multi-Language-Walkie-Talkie

![](./assets/system%20diagram%20v2%20big.drawio.png)

## Links to recent work ##
You can find me at:

https://bitbucket.org/eccdbb/ 

https://github.com/eccdbbabcdeee

https://www.linkedin.com/in/vincent-chyn-91874657/

Also, please check out my other projects below.  Many were intended for learning and getting a feel for the problem.

### Image Segmentation: ###
The objective is to segment car images into parts: (background, car, wheel, lights, window).  The approach is: use a pre-trained ResNet encoder followed by a Unet decoder.

https://bitbucket.org/eccdbb/image_segmentation/src/master/

### Multi-Stream ###
This is a video streaming web app.  Objective: let the viewer control the "camera perspective" (if there are multiple cameras in the room).

https://bitbucket.org/eccdbb/multi-stream/src/master/

### yandere-downloader ###
This is a download bot.  Give it a list of tags in a text file, and it will download them.

https://bitbucket.org/eccdbb/yandere-downloader/src/master/

### My Master's Thesis ###
The thesis and code can be found here:

https://bitbucket.org/eccdbb/vehicle_dynamics_thesis_ms/src/master/

### MPC (Python) ###
This is a re-implementation of my thesis code in Python.  The intent is to join together the solver (IPOPT), autodiff (JAX), and the MPC formulation.
Machine learning and learned warm starting comes later.  Architecturally, this project uses a microservices approach.  This is a work in progress.

https://bitbucket.org/eccdbb/mpc_kinematic/src/master/

### Autofocus DFD ###
This is a recent project intended to understand DFD.  I was attracted to DFD because of its similarity to MPC: open-loop calculation based on optimization.
It is a work in progress.

https://bitbucket.org/eccdbb/af_dfd/src/master/

### Stereo vision ###
This is an old project.  The goal was to understand and implement stereo vision in the quickest way possible.  Then, play with 3D data for tracking purposes.
Along the way, I learned Tensorflow/Keras, although it turns out this was a big detour.

https://bitbucket.org/eccdbb/multi-level_matching/src/master/

### Image warping ###
This is an old project.  The goal was to achieve arbitrary warping.  I used triangle intersection testing and barycentric interpolation.  This method does not have the limitation of invertible transformations.

https://bitbucket.org/eccdbb/image_warping_bary/src/master/

## Conclusion ##
Hopefully this was a helpful introduction to myself.  
Contact: see resume pdf.




